#!/bin/sh

set -xe

export KUBECONFIG="$(pwd)/kubeconfig"
echo "$KUBE_CA_PEM" > "$(pwd)/kube.ca.pem"

kubectl config set-cluster gitlab-deploy --server="$KUBE_URL" \
  --certificate-authority="$(pwd)/kube.ca.pem"

kubectl config set-credentials gitlab-deploy --token="$KUBE_TOKEN" \
  --certificate-authority="$(pwd)/kube.ca.pem"

kubectl config set-context gitlab-deploy \
  --cluster=gitlab-deploy --user=gitlab-deploy \
  --namespace="$KUBE_NAMESPACE"

kubectl config use-context gitlab-deploy

cat <<EOF | kubectl apply -f -
kind: Namespace
apiVersion: v1
metadata:
  name: $KUBE_NAMESPACE
EOF

cat <<EOF | kubectl apply -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: $CI_ENVIRONMENT_SLUG
  namespace: $KUBE_NAMESPACE
  labels:
    app: $CI_ENVIRONMENT_SLUG
spec:
  replicas: 1
  template:
    metadata:
      labels:
        name: $CI_ENVIRONMENT_SLUG
        app: $CI_ENVIRONMENT_SLUG
    spec:
      containers:
      - name: app
        image: $CI_REGISTRY_IMAGE:$CI_BUILD_REF_SLUG
        imagePullPolicy: Always
        ports:
        - name: web
          containerPort: 5000
EOF

cat <<EOF | kubectl apply -n $KUBE_NAMESPACE -f -
apiVersion: v1
kind: Service
metadata:
  name: $CI_ENVIRONMENT_SLUG
  namespace: $KUBE_NAMESPACE
  labels:
    app: $CI_ENVIRONMENT_SLUG
spec:
  ports:
    - name: web
      port: 5000
      targetPort: web
  selector:
    name: $CI_ENVIRONMENT_SLUG
EOF

cat <<EOF | kubectl apply -n $KUBE_NAMESPACE -f -
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: $CI_ENVIRONMENT_SLUG
  namespace: $KUBE_NAMESPACE
  labels:
    app: $CI_ENVIRONMENT_SLUG
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
spec:
  tls:
  - hosts:
    - $1
    secretName: ${CI_ENVIRONMENT_SLUG}-tls
  rules:
  - host: $1
    http:
      paths:
      - path: /
        backend:
          serviceName: $CI_ENVIRONMENT_SLUG
          servicePort: 5000
EOF

echo "Waiting for deployment..."
kubectl rollout status -n "$KUBE_NAMESPACE" -w "deployment/$CI_ENVIRONMENT_SLUG"
